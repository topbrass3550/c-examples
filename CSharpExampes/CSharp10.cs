﻿// C# 10 Global using directives
global using System.Threading.Tasks;
global using System;
global using System.Collections.Generic;
global using System.Linq;
global using System.Text;

// C# 10 – File-scoped namespace declaration
namespace CSharpExampes;

internal class CSharp10
{
    #region C# 10 Feature
    // C# 10 – Record structs
    public record struct Person0(string FirstName = "Aditya", string LastName = "Bhushan");
    public readonly record struct Person1(string name, int age);
    public record Person3(string Name, int Age = 34);

    // C# 10 Extended property patterns
    internal class Address
    {
        public string City { get; set; }
    }

    internal class Person
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public Address Address { get; set; }
    }

    private void ExtendedProp()
    {
        var obj = new Person
        {
            Name = "Aditya Bhushan",
            Age = 34,
            Address = new Address { City = "Gr. Noida" }
        };

        Console.Write($" Name : {obj.Name} Age: {obj.Age}");

        if (obj is Person { Address: { City: "Kanpur" } }) // Old
            Console.Write($"City: { obj.Address.City}");

        if (obj is Person { Address.City: "Delhi" }) // C# 10 Extended property pattern
            Console.Write($" City: { obj.Address.City}");
        Console.ReadKey();
    }

    // C# 10 Improvements of structure types
    internal record struct Persons
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public Address Address { get; set; }
    }
    private void ImprovementsofStructureTypes()
    {
        var per = new Persons { Name = "Rashmi", Age = 32, Address = new Address { City = "Kanpur" } };
        var p2 = per with { Name = "Rab Ji", Age = 32, Address = new Address { City = "Delhi" }};
    }
    #endregion
}
internal class CSharpOld
{
    #region Current Problem

    #endregion

}


